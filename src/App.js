// import logo from './logo.svg';
import './App.css';
import React from 'react';
import './styles/index.css';
import reportWebVitals from './reportWebVitals';
import Banner from './Banner';
import ShoppingList from './ShoppingList';


function App() {
  return (
    <React.Fragment>
      <Banner /><ShoppingList />
    </React.Fragment>
  );
}

reportWebVitals();

export default App;
