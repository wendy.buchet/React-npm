import React, { useState } from'react';

const plantList = [
    'monstera',
    'ficus lyrata',
    'pothos argenté',
    'yucca',
    'palmier'
]

//Revoir cette partie du cours sur les key et l'index
//https://openclassrooms.com/fr/courses/7008001-debutez-avec-react/7135593-gagnez-en-temps-et-en-efficacite-grace-aux-listes-et-aux-conditions


function ShoppingList() {

// Un input avec une valeur par défaut qui sera modifiable par le user
    let [input, setinput] = useState('');
//Un tableau avec les plantes qui sera modifié a chaque clique de bouton
    let [table, setTable]= useState([]);
//Quand je clique sur mon bouton, Si mon input n'est pas vide, ça mets à jour le tableau
    let majTable = () => {
        if (input!== '') {
            //Maj des données dans mon tableau
            setTable([...table, input]);
            //Maj de la liste de mon tableau(ancien élément + nouveau éléménet => ... propagation)
            //ajoute le nouvel input/element dans la liste de mon tableau
            setinput('');
        }
    }

    return (
        <div>
            <div id="saisieUtilisateur">
                <h2>Ajout d'éléments :</h2>
                <input type="text" id="inputFlowerName"
                onChange={(e)=>{input = e.target.value}}/>
                <br/>
                <br/>
                <button onClick={majTable}>SaveFlower</button>
            </div>
        
            <div>
                <h2>Liste d'éléments :</h2>
                <ul>
                {table.map((element, index) => (
                    <li key={index}>{element}</li>
                ))}
                </ul>
            </div>

            <div>   
                <h2>Autres exemples de fleurs :</h2>
                <ul>
                    {plantList.map((plant, index) => (
                        <li key={`${plant}-${index}`}>{ plant }</li>
                    ))}
                </ul>
            </div> 
        </div>         
    )
}



export default ShoppingList