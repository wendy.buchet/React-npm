import './styles/Banner.css';
import logo from './assets/logo.png'



function Header() {
    const title = 'Le petit botaniste'
    return (
        
        <div className='lmj-banner'>
            <img src={logo}  alt='La maison jungle' className='lmj-logo'/>
            <h1>{title}</h1>
            <h2>Mon composant Header apparait bien</h2>
        </div>
    )
}

function Description() {
return <p>Ici, créer une liste de plantes dont vous avez toujours rêvé 🌵🌱🎍</p>;
}

function Banner() {
    return (
      <div>
        <Header />
        <Description />
      </div>
    );
  }

export default Banner;